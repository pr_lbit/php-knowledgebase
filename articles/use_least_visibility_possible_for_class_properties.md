# Use least visibility possible for class properties

When declaring the visibility for members of a class, use the least required. The reasons are stated below:

* *Public properties* can be modified directly from outside the class. This is not secure, since the behaviour of a class object can be changed without the object ever knowing about it.
    * Eg: Imagine there is an object `$planeAutoPilot = new PlaneAutoPilot($originalRoute)`. Now, if the route is stored as a public property, some other unrelated code can simply change the route using `$planeAutoPilot->route = $hijackRoute` and the plane will be redirected without the autopilot ever noticing that the route has been changed. The changes will also not be logged.
* *Protected properties* can be accessed by it's child classes. This allows the child classes to read/modify the contents of the property, without really understanding how it is being used in the parent class.
    * Eg: You have a class `ProviderChannels`, with a `protected $discoveryChannelFrequency = "208.45MHz"`. A child class `MySubscriptionChannels extends ProviderChannels` can then modify the frequency of the `$discoveryChannelFrequency` to `"385.8MHz"` without the parent class ever knowing it. So, when the `MySubscriptionChannels` class calls a parent function `getDiscoveryChannelFrequency()`, it'll return `"385.8MHz"` instead of what's actually expected from the parent, which is `"208.45MHz"`.
* *Private properties* ensure that the variable cannot be accessed/modified from outside the class or its object. If by any chance you need to update this property's value you can use a method of the appropriate visibility. This will ensure that the class will always know when the value of its property is being changed. The method can also log the change if needed.

##### References
* [(Simplest answer) In object-oriented programming, why is it bad practice to make data members public when the get() & set() public members modify it anyway?](https://www.quora.com/In-object-oriented-programming-why-is-it-bad-practice-to-make-data-members-public-when-the-get-set-public-members-modify-it-anyway/answer/Steve-Zara-1)
