# Class constructors in OOP should not return any value

The constructor of a class, in object oriented languages, does not return anything when invoked during object creation. Not even `void`.

It can however return a value if the `return` statement is present in the constructor, and the constructor is manually invoked (eg: `$object->__construct()`). But this is not a good practice and is not recommended.

**So, you should not add a return type for constructors either as type hint (`: void`) or in the docblock (`@return void`).**

![PHP error when declaring class with a constructor having 'void' return type](/images/2022-04-06_120444-Error_when_adding_return_type_to_calss_constructor.png "PHP error when declaring class with a constructor having 'void' return type")

##### References:
* [How to specify the void return type for a constructor](https://stackoverflow.com/questions/61969897/how-to-specify-the-void-return-type-for-a-constructor)
* [Returning value from constructor using initialized object](https://stackoverflow.com/a/11904285/3046322)
* [Request for return type hint for constructor closed as 'Not a bug' by PHP team](https://bugs.php.net/bug.php?id=75263)
