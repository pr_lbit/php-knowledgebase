# PHPUnit: Avoid setting test data from `setUp`

Setting the sample data from the `setUp` does not do much in terms of *speed*, since the `setUp` function is called for each test anyway. But it reduces the readability of the subsequent test functions, because the reader might not know what is already stored.

So, it is better to write this function at the beginning of each of the test functions.

Also, the sample data does not have to be set for every function, *for example: when testing for exceptions thrown due to missing data.*
