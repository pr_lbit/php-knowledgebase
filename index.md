# Index

* [Constructors should not return any value](/articles/constructors_should_not_return_any_value.md)
* [PHPUnit: Avoid setting test data from `setUp`](/articles/avoid_setting_test_data_from_setUp.md)
* [Use least visibility possible for class properties](/articles/use_least_visibility_possible_for_class_properties.md)
